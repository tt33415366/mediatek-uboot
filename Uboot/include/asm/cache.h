/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1997, 98, 99, 2000, 2003 Ralf Baechle
 * Copyright (C) 1999 Silicon Graphics, Inc.
 */
#ifndef _ASM_CACHE_H
#define _ASM_CACHE_H

#define CONFIG_SYS_CACHELINE_SIZE   32
/*
 * The maximum L1 data cache line size on MIPS seems to be 128 bytes.  We use
 * that as a default for aligning DMA buffers unless the board config has
 * specified another cache line size.
 */
#ifdef CONFIG_SYS_CACHELINE_SIZE
#define ARCH_DMA_MINALIGN   CONFIG_SYS_CACHELINE_SIZE
#else
#define ARCH_DMA_MINALIGN   128
#endif

#endif /* _ASM_CACHE_H */
