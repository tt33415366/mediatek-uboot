#ifndef DHCPD_DEBUG
#define DHCPD_DEBUG LWIP_DBG_OFF
#endif /* DHCPD_DEBUG */

#include "lwip/opt.h"

#include "lwip/mem.h"
#include "lwip/udp.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "lwip/def.h"
#include "lwip/dhcp.h"
#include "lwip/etharp.h"
#include "lwip/debug.h"
#include "lwip/sys.h"

#include <string.h>

#define DHCPD_BOOTREQUEST 1
#define DHCPD_BOOTREPLY	  2
#define DHCPD_SRVPORT 67
#define DHCPD_CLIPORT 68

#define DHCP_MAGIC              0x63825363

/* DHCP_MESSAGE_TYPE values */
#define DHCPDISCOVER            1 /* client -> server */
#define DHCPOFFER               2 /* client <- server */
#define DHCPREQUEST             3 /* client -> server */
#define DHCPDECLINE             4 /* client -> server */
#define DHCPACK                 5 /* client <- server */
#define DHCPNAK                 6 /* client <- server */
#define DHCPRELEASE             7 /* client -> server */
#define DHCPINFORM              8 /* client -> server */
#define DHCP_MINTYPE DHCPDISCOVER
#define DHCP_MAXTYPE DHCPINFORM


#define DHCP_REQUESTED_IP       0x32 /* sent by client if specific IP is wanted */
#define DHCP_LEASE_TIME         0x33
#define DHCP_OPTION_OVERLOAD    0x34
#define DHCP_MESSAGE_TYPE       0x35
#define DHCP_SERVER_ID          0x36 /* by default server's IP */
#define DHCP_PARAM_REQ          0x37 /* list of options client wants */


typedef struct _bootp {
	u8_t op, htype, hlen, hops;
	u8_t xid[4];
	u16_t secs, flags;
	u32_t ciaddr;
	u32_t yiaddr;
	u32_t siaddr_nip;
	u32_t gateway_nip;
	u8_t chaddr[6];
	u8_t chaddrpad[10];
	u8_t sname[64];
	u8_t bfname[128];
	u32_t cookie;
#if __STDC_VERSION__ >= 199901L
	/* C99 */
	u8_t options[];
#else
	u8_t options[0];
#endif 
}__attribute__((packed)) bootp_t ;

typedef struct _dhcp_opt {
	u8_t type;
	u8_t len;
#if __STDC_VERSION__ >= 199901L
	/* C99 */
	u8_t option[];
#else
	u8_t option[0];
#endif
} __attribute__((packed)) dhcp_opt_t ;

typedef struct _dhcp_entry {
	u32_t nip;
	u8_t mac[6];
	/* A short expire for offser sending, normal expire while we received an request */
	u32_t expires;
} dhcp_entry_t;

static struct dhcp_svr_config {
	u32_t svr_nip;
	u32_t start_ip;
	u32_t end_ip;
	u32_t subnet_mask;
	u32_t lease_time;
} dhcpd_config = {
	.subnet_mask = 0xffffff00,
	.lease_time = 3600 /* 3600s */
};
static dhcp_entry_t dhcp_pools[100] = {{0}};

static void nip_dump(u32_t nip)
{
	ip_addr_t _dip;
	
	ip_addr_set_ip4_u32(&_dip, nip);
	LWIP_DEBUGF(DHCPD_DEBUG, ("\tip = %s\n", ipaddr_ntoa(&_dip)));
}

static dhcp_entry_t *find_dhcp_lease_by_mac(u8_t *mac)
{
	int i;
	
	for (i = 0; i < LWIP_ARRAYSIZE(dhcp_pools); i++) {
		if (0 == memcmp(dhcp_pools[i].mac, mac, sizeof(dhcp_pools[i].mac))) {
			return &dhcp_pools[i];
		}
	}

	return NULL;
}
static dhcp_entry_t *find_dhcp_lease_by_nip(u32_t ip) __attribute__((unused));

static dhcp_entry_t *find_dhcp_lease_by_nip(u32_t nip)
{
	int i;
	ip_addr_t ip = IPADDR4_INIT(nip);

	for (i = 0; i < LWIP_ARRAYSIZE(dhcp_pools); i++) {
		if (dhcp_pools[i].nip == 0
			|| 0 == memcmp(&dhcp_pools[i].nip, &ip, sizeof(ip))) {
			return &dhcp_pools[i];
		}
	}
	
	return NULL;
}


static int is_expired_lease(dhcp_entry_t *lease)
{
	return (lease->expires < sys_now() * 1000);
}

static void update_lease_expires(dhcp_entry_t *lease, u32_t new_expires)
{
	lease->expires = (u32_t)(sys_now() * 1000) + new_expires;
}

/* Find a new usable (we think) address */
static u32_t find_free_or_expired_nip(const u8_t *safe_mac)
{
	u32_t addr;
	dhcp_entry_t *oldest_lease = NULL;
	u32_t nip;

	u32_t stop;
	unsigned i, hash;

	/* hash hwaddr: use the SDBM hashing algorithm.  Seems to give good
	 * dispersal even with similarly-valued "strings".
	 */
	hash = 0;
	for (i = 0; i < 6; i++)
		hash += safe_mac[i] + (hash << 6) + (hash << 16) - hash;

	/* pick a seed based on hwaddr then iterate until we find a free address. */
	addr = dhcpd_config.start_ip
		+ (hash % (1 +  dhcpd_config.end_ip - dhcpd_config.start_ip));
	stop = addr;
	
	do {
		dhcp_entry_t *lease;

		/* ie, 192.168.55.0 */
		if ((addr & 0xff) == 0)
			goto next_addr;
		/* ie, 192.168.55.255 */
		if ((addr & 0xff) == 0xff)
			goto next_addr;
		nip = htonl(addr);
		/* skip our own address */
		if (nip == dhcpd_config.svr_nip)
			goto next_addr;

		nip_dump(nip);
		
		lease = find_dhcp_lease_by_nip(nip);
		if (!lease) {
			return nip;
		} else if (is_expired_lease(lease)) {
			memset(lease, 0x0, sizeof(*lease));
			return nip;
		} else if (!oldest_lease || oldest_lease->expires > lease->expires) {
			oldest_lease = lease;
		}

 next_addr:
		addr++;
		if (addr > dhcpd_config.end_ip)
			addr = dhcpd_config.start_ip;
	} while (addr != stop);
	
	nip = oldest_lease->nip;
	memset(oldest_lease, 0x0, sizeof(*oldest_lease));
	
	return nip;
}

static dhcp_entry_t *add_dhcp_lease(u32_t nip, u8_t *mac)
{
	int i;
	dhcp_entry_t *oldest = NULL;

	for (i = 0; i < LWIP_ARRAYSIZE(dhcp_pools); i++) {
		if (dhcp_pools[i].nip == 0
			|| is_expired_lease(&dhcp_pools[i])) {
			dhcp_pools[i].nip = nip;
			memcpy(dhcp_pools[i].mac, mac, sizeof(dhcp_pools[i].mac));
			return &dhcp_pools[i];
		} else if (oldest || oldest->expires > dhcp_pools[i].expires) {
			oldest = &dhcp_pools[i];
		}
	}

	if (oldest) {
		/* Reuse the oldest one */
		memset(oldest, 0x0, sizeof(*oldest));
		 oldest->nip =  nip;
		memcpy(oldest->mac, mac, sizeof(oldest->mac));
	}
	
	return oldest;
}

static dhcp_opt_t *__dhcp_opt_end(u8_t *buff)
{
	dhcp_opt_t *op = (dhcp_opt_t *)buff;
	
	while (op && op->type != 0xff) {
		op = (dhcp_opt_t *)(((u8_t *)op) +  sizeof(*op) + op->len);
	}

	return op;
}


static  dhcp_opt_t * __add_dhcp_opt(dhcp_opt_t *op, u8_t type, void *opt, u8_t opt_len)
{
	LWIP_ASSERT("This is not the last option", op->type == 0xff);
	
	op->type = type;
	op->len = opt_len;
	memcpy(op->option, opt, opt_len);
	op = (dhcp_opt_t *)(((u8_t *)op) + sizeof(*op) + op->len);
	op->type = 0xff;

	return op;
}
static int add_dhcp_opt(u8_t *buff, u8_t type, u8_t *opt, u8_t opt_len) __attribute__((unused));

static int add_dhcp_opt(u8_t *buff, u8_t type, u8_t *opt, u8_t opt_len)
{
	dhcp_opt_t *op = __dhcp_opt_end(buff);
	
	__add_dhcp_opt(op, type, opt, opt_len);
	
	return 0;	
}

static inline dhcp_opt_t *__get_dhcp_opt(u8_t *buff, int *offset)
{
	dhcp_opt_t *op = (dhcp_opt_t *)(buff + *offset);
	
	switch (op->type) {
		case 0xff:
			/* Option End */
			op = NULL;
			break;
		default:
			break;
	}
	if (op) {
		*offset += sizeof(*op) + op->len;
	}

	return op;
}

static u8_t *get_dhcp_opt(u8_t *buff, int type)
{
	int offset = 0;
	dhcp_opt_t *op = NULL;
	
	while ((op = __get_dhcp_opt(buff, &offset)) != NULL) {
		if (op->type == type) {
			break;
		}
	}
	
	return op ? op->option : NULL;
}

static dhcp_opt_t *__init_header(bootp_t *bootp, u8_t type)
{
	bootp->op = DHCPD_BOOTREQUEST; /* if client to a server */
	switch (type) {
	case DHCPOFFER:
	case DHCPACK:
	case DHCPNAK:
		bootp->op = DHCPD_BOOTREPLY; /* if server to client */
	}
	bootp->htype = 1; /* ethernet */
	bootp->hlen = 6;
	bootp->cookie = htonl(DHCP_MAGIC);
	bootp->options[0] = 0xff;
	return __add_dhcp_opt((dhcp_opt_t *)bootp->options, DHCP_MESSAGE_TYPE, &type, sizeof(type));
}

static dhcp_opt_t * init_packet(u8_t *buff, bootp_t *bootp, u8_t type)
{
	bootp_t *_bootp = (bootp_t *)buff;

	memcpy(buff, bootp, sizeof(*bootp));
	/* yiaddr not set */
	_bootp->yiaddr = ip_addr_get_ip4_u32(IP_ADDR_ANY);
	return __init_header(_bootp, type);
}

static void send_offer(struct udp_pcb *pcb, bootp_t *bootp, dhcp_entry_t *lease, u32_t *requested_ip_opt)
{
	u8_t buff[sizeof(*bootp) + 1024] = {0};
	bootp_t *_bootp = (bootp_t *)buff;
	u32_t lease_time = htonl(dhcpd_config.lease_time);
	u32_t subnet_mask = htonl(dhcpd_config.subnet_mask);
	struct pbuf *p;
	dhcp_opt_t *op = (dhcp_opt_t *)_bootp->options;
		
	op = init_packet(buff, bootp, DHCPOFFER);
	op = __add_dhcp_opt(op, DHCP_SERVER_ID, &dhcpd_config.svr_nip, sizeof(dhcpd_config.svr_nip));
	op = __add_dhcp_opt(op, DHCP_LEASE_TIME, &lease_time, sizeof(lease_time));
	op = __add_dhcp_opt(op, 0x01, &subnet_mask, sizeof(subnet_mask));
	
	/* TODO: 
	  *   Add gateway options 
	  */
	
	if (requested_ip_opt) {
		u32_t req_nip = *requested_ip_opt;
		if ( ntohl(req_nip) >= dhcpd_config.start_ip
		 && ntohl(req_nip) <= dhcpd_config.end_ip) {
			dhcp_entry_t *lease = find_dhcp_lease_by_nip(req_nip);

			if (!lease ||  (is_expired_lease(lease))) {
				_bootp->yiaddr = req_nip;
			} 
		}
	}
	
	if (_bootp->yiaddr == ip_addr_get_ip4_u32(IP_ADDR_ANY)) {
		_bootp->yiaddr = find_free_or_expired_nip(bootp->chaddr);
	}

	lease = add_dhcp_lease(_bootp->yiaddr, _bootp->chaddr);
	/* Update 60 * 3 for new */
	update_lease_expires(lease, 60 * 3);

	/* Add the last length */
	op++;
	LWIP_DEBUGF(DHCPD_DEBUG, ("allocating pbuf with length = %d\n", (u8_t *)op - buff));
	LWIP_ASSERT("New length must be less then the total buffer length", (u8_t *)op - buff <= sizeof(buff));
	p = pbuf_alloc(PBUF_TRANSPORT, (u8_t *)op - buff, PBUF_RAM);
	if (p) {
		pbuf_take(p, buff, (u8_t *)op - buff);
		udp_sendto(pcb, p, IP_ADDR_BROADCAST, DHCPD_CLIPORT);
		pbuf_free(p);
	}
	
}

static void send_ack(struct udp_pcb *pcb, bootp_t *bootp, dhcp_entry_t *lease, u32_t *requested_ip_opt)
{
	u8_t buff[sizeof(*bootp) + 1024] = {0};
	bootp_t *_bootp = (bootp_t *)buff;
	u32_t lease_time = htonl(dhcpd_config.lease_time);
	struct pbuf *p = NULL;
	dhcp_opt_t *op = (dhcp_opt_t *)_bootp->options;

	/* FIXME: Use add_dhcp_opt to release the init_packet's return value */
	op = init_packet(buff, bootp, DHCPACK);
	_bootp->yiaddr =  lease->nip;
	op = __add_dhcp_opt(op, DHCP_SERVER_ID, &dhcpd_config.svr_nip, sizeof(dhcpd_config.svr_nip));
	op = __add_dhcp_opt(op, DHCP_LEASE_TIME, &lease_time, sizeof(lease_time));

	/* Add the last length */
	op++;
	LWIP_DEBUGF(DHCPD_DEBUG, ("allocating pbuf with length = %d\n", (u8_t *)op - buff));
	LWIP_ASSERT("New length must be less then the total buffer length", (u8_t *)op - buff <= sizeof(buff));
	p = pbuf_alloc(PBUF_TRANSPORT, (u8_t *)op - buff, PBUF_RAM);
	if (p) {
		pbuf_take(p, buff, (u8_t *)op - buff);
		udp_sendto(pcb, p, IP_ADDR_BROADCAST, DHCPD_CLIPORT);
		pbuf_free(p);
	}
	
	update_lease_expires(lease, dhcpd_config.lease_time);
}

static void send_nak(struct udp_pcb *pcb, bootp_t *bootp, dhcp_entry_t *lease, u32_t *requested_ip_opt)
{
	u8_t buff[sizeof(*bootp) + 1024] = {0};
	bootp_t *_bootp = (bootp_t *)buff;
	struct pbuf *p = NULL;
	dhcp_opt_t *op = (dhcp_opt_t *)_bootp->options;
	
	op = init_packet(buff, bootp, DHCPNAK);
	op = __dhcp_opt_end(_bootp->options);

	/* Add the last length */
	op++;
	LWIP_ASSERT("New length must be less then the total buffer length", (u8_t *)op - buff <= sizeof(buff));
	p = pbuf_alloc(PBUF_TRANSPORT, (u8_t *)op - buff, PBUF_RAM);
	if (p) {
		pbuf_take(p, buff, (u8_t *)op - buff);
		udp_sendto(pcb, p, IP_ADDR_BROADCAST, DHCPD_CLIPORT);
		pbuf_free(p);
	}
}

static void send_inform(struct udp_pcb *pcb, bootp_t *bootp, dhcp_entry_t *lease, u32_t *requested_ip_opt)
{
	u8_t buff[sizeof(*bootp) + 1024] = {0};
	bootp_t *_bootp = (bootp_t *)buff;
	struct pbuf *p = NULL;
	dhcp_opt_t *op = (dhcp_opt_t *)_bootp->options;
	
	op = init_packet(buff, bootp, DHCPACK);
	op = __dhcp_opt_end(_bootp->options);

	/* Add the last length */
	op++;
	LWIP_ASSERT("New length must be less then the total buffer length", (u8_t *)op - buff <= sizeof(buff));
	p = pbuf_alloc(PBUF_TRANSPORT, (u8_t *)op - buff, PBUF_RAM);
	if (p) {
		pbuf_take(p, buff, (u8_t *)op - buff);
		udp_sendto(pcb, p, IP_ADDR_BROADCAST, DHCPD_CLIPORT);
		pbuf_free(p);
	}
	
}

static void dhcpd_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p,
    const ip_addr_t *addr, u16_t port)
{
#define MIN(_a, _b) ((_a) < (_b) ? (_a) : (_b))

	LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s:%u ]\n", __func__, __LINE__));

	bootp_t *bootp = NULL;
	u8_t buff[sizeof(*bootp) + 1024] = {0};

	if (p->tot_len > sizeof(*bootp)) {
		pbuf_copy_partial(p, buff, MIN(p->tot_len, sizeof(buff)), 0);
		bootp = (bootp_t *)buff;
	} else {
		LWIP_DEBUGF(DHCPD_DEBUG, ("Not a bootp packet, ignore it\n"));
	}

	if (bootp->op != DHCPD_BOOTREQUEST) {
		bootp = NULL;
		LWIP_DEBUGF(DHCPD_DEBUG, ("Not a REQUEST, ignore it\n"));
	}
	
	if (bootp->hlen != 6) {
		bootp = NULL;
		LWIP_DEBUGF(DHCPD_DEBUG, ("MAC addr length not 6, ignore it\n"));
	}

	if (bootp->cookie != htonl(DHCP_MAGIC)) {
		LWIP_DEBUGF(DHCPD_DEBUG, ( "%x is not DHCP magic\n", bootp->cookie));
		bootp = NULL;
	}
	
	if (bootp) {
		u8_t *server_id_opt;
		u8_t *requested_ip_opt;
		u8_t *state = get_dhcp_opt(bootp->options, DHCP_MESSAGE_TYPE);
		u32_t requested_nip = 0;
		dhcp_entry_t *lease = NULL;
		

		if (state == NULL || state[0] < DHCP_MINTYPE || state[0] > DHCP_MAXTYPE) {
			LWIP_DEBUGF(DHCPD_DEBUG, ("no or bad message type option, ignoring packet\n"));
			goto cleanup;
		}
	
		/* Get SERVER_ID if present */
		server_id_opt = get_dhcp_opt(bootp->options, DHCP_SERVER_ID);
		if (server_id_opt) {
			u32_t server_id_network_order;
			
			server_id_network_order = *((u32_t *)server_id_opt);
			if (server_id_network_order != dhcpd_config.svr_nip) {
				/* client talks to somebody else */
				LWIP_DEBUGF(DHCPD_DEBUG, ("server ID doesn't match, ignoring\n"));
				goto cleanup;
			}
		}
		
		lease = find_dhcp_lease_by_mac(bootp->chaddr);
		/* Get REQUESTED_IP if present */
		requested_ip_opt = get_dhcp_opt(bootp->options, DHCP_REQUESTED_IP);
		if (requested_ip_opt) {
			memcpy(&requested_nip, requested_ip_opt, sizeof(requested_nip));
		}
		
		switch (state[0]) {
			case DHCPDISCOVER: /* DHCP Offer */
				LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s:%u ] Sending DHCP Offer ...\n", __func__, __LINE__));
				send_offer(pcb, bootp, lease, &requested_nip);
				break;
			case DHCPREQUEST: 
				if (!requested_ip_opt) {
					requested_nip = bootp->ciaddr;
					if (requested_nip == 0) {
						LWIP_DEBUGF(DHCPD_DEBUG, ("no requested IP and no ciaddr, ignoring"));
						break;
					}
				}
				if (lease && requested_nip == lease->nip) {
					/* client requested or configured IP matches the lease.
					 * ACK it, and bump lease expiration time. */
					/* DHCP ACK */
					LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s:%u ] Sending DHCP ACK ...\n", __func__, __LINE__));
					send_ack(pcb, bootp, lease, &requested_nip);
				} else if (server_id_opt    /* client is in SELECTING state */
				 || requested_ip_opt /* client is in INIT-REBOOT state */
				) {
					/* DHCP NAK */
					LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s:%u ] Sending DHCP NAK ...\n", __func__, __LINE__));
					send_nak(pcb, bootp, lease, &requested_nip);
				}
				break;
			case DHCPINFORM:
				LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s:%u ] Sending DHCPINFORM ...\n", __func__, __LINE__));
				send_inform(pcb, bootp, lease, &requested_nip);
				break;
			default:
				LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s:%u ] Unsupported options\n", __func__, __LINE__));
				break;
		}
	}
	
cleanup:
	/* Cleanup */
	/* udp_input said we should manually free the pbuf passed to us */
	pbuf_free(p);
}

int dhcpd_init(ip_addr_t svr_ip)
{
	static struct udp_pcb *pcb = NULL;
	err_t err;
	u32_t ip;
	ip_addr_t _dip = {0};

	LWIP_DEBUGF(DHCPD_DEBUG, ("[ %s ]  %s (%u)\n", __func__, ipaddr_ntoa(&svr_ip), ip_addr_get_ip4_u32(&svr_ip)));
	
	dhcpd_config.svr_nip = ip_addr_get_ip4_u32(&svr_ip);  /* Seems that it's already in network byte order*/
	ip = ntohl(dhcpd_config.svr_nip);
	dhcpd_config.start_ip = (ip  & dhcpd_config.subnet_mask)  + 100; /* Start from xxx.xxx.xxx.100 */
	dhcpd_config.end_ip = (ip & dhcpd_config.subnet_mask) + 200; /* End with xxx.xxx.xxx.200 */


	ip_addr_set_ip4_u32(&_dip, htonl(dhcpd_config.start_ip));
	LWIP_DEBUGF(DHCPD_DEBUG, (" startip = %s\n", ipaddr_ntoa(&_dip)));	
	ip_addr_set_ip4_u32(&_dip, htonl(dhcpd_config.end_ip));
	LWIP_DEBUGF(DHCPD_DEBUG, (" endip = %s\n", ipaddr_ntoa(&_dip)));

	if (!pcb) {
		pcb = udp_new_ip_type(IPADDR_TYPE_ANY);
		err = udp_bind(pcb, IP_ADDR_ANY, DHCPD_SRVPORT);
	  	LWIP_ASSERT("dhcpd_init: udp_bind failed", err == ERR_OK);
	  	
		udp_recv(pcb, dhcpd_recv, NULL);
	}
	
	return err;
}
