#include <cc.h>

u32_t sys_jiffies(void)
{
	extern unsigned long mips_count_get(void);

	return mips_count_get();
}
/* Return in ms */
u32_t sys_now(void)
{
	/* FIXME: use CPU FREQUENCY to get a more accurate time */
	return sys_jiffies() / 1000;
}
