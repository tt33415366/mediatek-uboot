#ifndef __STDLIB_H__
#define __STDLIB_H__
extern void *malloc(size_t size);
extern void free(void *ptr);
#endif /* __STDLIB_H__ */
