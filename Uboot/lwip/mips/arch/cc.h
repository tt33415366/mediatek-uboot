#ifndef __CC_H__
#define __CC_H__
/* Empty for now */
/*
 * The following data structure is placed in some memory wich is
 * available very early after boot (like DPRAM on MPC8xx/MPC82xx, or
 * some locked parts of the data cache) to allow for a minimum set of
 * global variables during system initialization (until we have set
 * up the memory controller so that we can use RAM).
 *
 * Keep it *SMALL* and remember to set CFG_GBL_DATA_SIZE > sizeof(gd_t)
 */

typedef	struct	global_data {
	/*bd_t */void	*bd;
	unsigned long	flags;
	unsigned long	baudrate;
	unsigned long	have_console;	/* serial_init() was called */
	unsigned long	ram_size;	/* RAM size */
	unsigned long	reloc_off;	/* Relocation Offset */
	unsigned long	env_addr;	/* Address  of Environment struct */
	unsigned long	env_valid;	/* Checksum of Environment valid? */
	void		**jt;		/* jump table */
} gd_t;

/*
 * Global Data Flags
 */
#define	GD_FLG_RELOC	0x00001		/* Code was relocated to RAM     */
#define	GD_FLG_DEVINIT	0x00002		/* Devices have been initialized */
#define	GD_FLG_SILENT	0x00004		/* Silent mode			 */

#define DECLARE_GLOBAL_DATA_PTR     register volatile gd_t *gd asm ("k0")

#define LWIP_PLATFORM_DIAG(x)	do {printf x;} while(0)
#define LWIP_PLATFORM_ASSERT(x) do {printf("Assertion \"%s\" failed at line %d in %s\n", \
		 x, __LINE__, __FILE__); while(1); } while (0)
#define LWIP_NO_STDINT_H				1
#define LWIP_NO_INTTYPES_H				1
#define LWIP_NO_LIMITS_H				1
typedef unsigned char	u8_t;
typedef signed char	s8_t;
typedef unsigned short	u16_t;
typedef signed short	s16_t;
typedef unsigned int	u32_t;
typedef signed int	s32_t;
typedef unsigned long mem_ptr_t;
//#define LWIP_NO_STDDEF_H 1
#ifndef size_t
//typedef unsigned long size_t;
#endif /* size_t */
extern int printf(const char *format, ...);
/* U16_F, S16_F, X16_F, U32_F, S32_F, X32_F, SZT_F */
#define		X8_F	"hhx"
#define		S16_F   "hd"
 
#define		S32_F   "d"
 
#define		U16_F   "hu"
 
#define		U32_F   "u"
 
#define		X16_F   "hx"
 
#define		X32_F   "x"
#endif /* __CC_H__ */
