//#include <string.h>
/* lwIP headers */
#include "lwip/init.h"
#include "lwip/stats.h"
#include "lwip/netif.h"
#include "lwip/pbuf.h"
#include "lwip/snmp.h"
#include "lwip/dhcp.h"
#include "lwip/etharp.h"
#include "lwip/ip4_addr.h"
#include "lwip/apps/httpd.h"
#include "lwip/timeouts.h"
/* Uboot headers */
#if 0
#include "common.h"
#include "net.h"
#else
typedef unsigned char uchar;
extern volatile uchar * NetTxPacket;		/* THE transmit packet		*/
extern volatile uchar * NetRxPkt;		/* Current receive packet	*/
extern int		NetRxPktLen;		/* Current rx packet length	*/
extern int dev_init(void);
extern int eth_send(volatile void *packet, int length);	   /* Send a packet	*/
extern int eth_rx(void);			/* Check for received packets	*/
const char *eth_get_macaddr(void);
#endif

#undef DEBUG
#ifdef DEBUG
#define ULWIP_DEBUG(_fmt, ...) \
	printf("[ LwIP ] [ %s:%u ] "_fmt, __func__, __LINE__, ##__VA_ARGS__)
#else
#define ULWIP_DEBUG
#endif /* DEBUG */

static struct pbuf *
eth_recv(void)
{
  /* Service MAC IRQ here */
  int rx = eth_rx();

  ULWIP_DEBUG("rx = %d\n", rx);

  if (rx) {
	  /* Allocate pbuf from pool (avoid using heap in interrupts) */
	  struct pbuf* p = pbuf_alloc(PBUF_RAW, /* eth_data_count */ NetRxPktLen, PBUF_POOL);

	  if(p != NULL) {
		  /* Copy ethernet frame into pbuf */
		  pbuf_take(p, /* eth_data */ NetRxPkt, /* eth_data_count */NetRxPktLen);
	  }
		return p;
  }

  return NULL;
}


static err_t 
netif_output(struct netif *netif, struct pbuf *p)
{
  LINK_STATS_INC(link.xmit);

  /* Update SNMP stats (only if you use SNMP) */
  MIB2_STATS_NETIF_ADD(netif, ifoutoctets, p->tot_len);
  int unicast = ((((char *)p->payload)[0] & 0x01) == 0);
  if (unicast) {
    MIB2_STATS_NETIF_INC(netif, ifoutucastpkts);
  } else {
    MIB2_STATS_NETIF_INC(netif, ifoutnucastpkts);
  }
  /* Ensure NetTxPacket is not NULL */
  pbuf_copy_partial(p, /* mac_send_buffer */ NetTxPacket, p->tot_len, 0);
  /* Start MAC transmit here */
  eth_send(NetTxPacket, p->tot_len);

  return ERR_OK;
}

static void 
netif_status_callback(struct netif *netif)
{
  ULWIP_DEBUG("netif status changed %s\n", ip4addr_ntoa(netif_ip4_addr(netif)));
}

static err_t 
__netif_init(struct netif *netif)
{
  ULWIP_DEBUG("netif = 0x%p\n", netif);

  netif->linkoutput = netif_output;
  netif->output     = etharp_output;
  netif->mtu        = /* ETHERNET_MTU */1500;
  netif->flags      = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_ETHERNET | NETIF_FLAG_IGMP | NETIF_FLAG_MLD6;
  MIB2_INIT_NETIF(netif, snmp_ifType_ethernet_csmacd, 100000000);


  SMEMCPY(netif->hwaddr, /* your_mac_address_goes_here */eth_get_macaddr(), sizeof(netif->hwaddr));
  netif->hwaddr_len = sizeof(netif->hwaddr);

  return ERR_OK;
}

void 
uboot_ent_main(void)
{
  DECLARE_GLOBAL_DATA_PTR;

  ip4_addr_t ipaddr;
  ip4_addr_t netmask; 
  ip4_addr_t gw; 
  struct netif netif;
  char buff[32];

  dev_init();

  lwip_init();

  ULWIP_DEBUG(" running...\n");
  IP4_ADDR(&ipaddr, 10, 10, 10, 128);
  IP4_ADDR(&gw, 10, 10, 10, 1);
  IP4_ADDR(&netmask, 255, 255, 255, 0);
  netif_add(&netif, /* IP4_ADDR_ANY */&ipaddr, /*IP4_ADDR_ANY */&netmask, /* IP4_ADDR_ANY */&gw, NULL, __netif_init, netif_input);
  netif.name[0] = 'e';
  netif.name[1] = '0';
#if LWIP_NETIF_STATUS_CALLBACK
  netif_set_status_callback(&netif, netif_status_callback);
#endif /* LWIP_NETIF_STATUS_CALLBACK */
  netif_set_default(&netif);
  netif_set_up(&netif);
  
  ULWIP_DEBUG("start dhcpc...\n");
  /* Start DHCP and HTTPD */
  //dhcp_start(&netif );
  extern int dhcpd_init(ip_addr_t svr_ip);
  dhcpd_init(ipaddr);
  ULWIP_DEBUG("start httpd...\n");
  httpd_init();

  netif_set_link_up(&netif);
  while(1) {
	ULWIP_DEBUG("running...\n");
#ifdef LINK_STATUS
    /* Check link state, e.g. via MDIO communication with PHY */
    if(link_state_changed()) {
      if(link_is_up()) {
        netif_set_link_up(&netif);
      } else {
        netif_set_link_down(&netif);
      }
    }
#endif

    /* Check for received frames, feed them to lwIP */
    struct pbuf* p = eth_recv();
    if(p != NULL) {
      LINK_STATS_INC(link.recv);
 
      /* Update SNMP stats (only if you use SNMP) */
      MIB2_STATS_NETIF_ADD(&netif, ifinoctets, p->tot_len);
      int unicast = ((((char *)p->payload)[0] & 0x01) == 0);
      if (unicast) {
        MIB2_STATS_NETIF_INC(&netif, ifinucastpkts);
      } else {
        MIB2_STATS_NETIF_INC(&netif, ifinnucastpkts);
      }

      if(netif.input(p, &netif) != ERR_OK) {
        pbuf_free(p);
      }
    }
     
    /* Cyclic lwIP timers check */
    sys_check_timeouts();
     
    /* your application goes here */
  }
}
